export type Trouter = {
  splash: undefined;
  main: undefined;
};

export type MainApp = {
  home: undefined;
  account: undefined;
  search: undefined;
  videos: undefined;
};
