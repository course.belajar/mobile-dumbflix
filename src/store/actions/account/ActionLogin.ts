import {API_DEV} from '@env';
import {Dispatch} from 'redux';
import {
  ActionLogin,
  ActionType,
} from '../../../interfaces/redux/account/ilogin';

const API = () =>
  fetch(`${API_DEV}/users`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  }).then(res => res.json());

export default () => async (dispatch: Dispatch<ActionLogin>) => {
  try {
    dispatch({type: ActionType.ACTION_LOGIN_BEGIN});
    const result = await API();
    if (result.status !== 200) {
      dispatch({
        type: ActionType.ACTION_LOGIN_ERROR,
        error: 'username/password',
      });
    }

    dispatch({
      type: ActionType.ACTION_LOGIN_SUCCESS,
      success: result.data,
    });
  } catch (err) {
    dispatch({
      type: ActionType.ACTION_LOGIN_ERROR,
      error: err,
    });
  }
};
