import {AnyAction} from 'redux';

const intialState = {
  loading: false,
  success: null,
  error: false,
};

export default (state = intialState, action: AnyAction) => {
  switch (action.type) {
    case 'ACTION_LOGIN_BEGIN':
      return {
        ...state,
        loading: true,
        success: null,
      };
    case 'ACTION_LOGIN_SUCCESS':
      return {
        ...state,
        loading: false,
        success: action.success,
      };

    case 'ACTION_LOGIN_ERROR':
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};
