import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

import globalStyle from '../../styles/global';

const AccountScreen = () => {
  return (
    <View style={globalStyle.Container}>
      <Text style={styles.Text}>AccountScreen</Text>
    </View>
  );
};

export default AccountScreen;

const styles = StyleSheet.create({
  Text: {
    color: 'red',
  },
});
