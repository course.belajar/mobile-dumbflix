import {Text, View, ImageBackground, TouchableOpacity} from 'react-native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import React from 'react';
import globalStyle from '../../styles/global';
import {Thome} from '../../types/routetype/home';
import {datafilm2} from '../home/fake';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './style';

type Props = NativeStackScreenProps<Thome, 'detail'>;

const DetailFilm = (route: Props) => {
  const id = route.route.params.userId;
  const find = datafilm2.find(item => item.id === id);
console.log(find);

  return (
    <View style={globalStyle.Container}>
      <ImageBackground
        source={find?.img}
        style={styles.imgs}
        imageStyle={styles.ImageStyle}>
        <TouchableOpacity>
          {/* <View style={styles.WrapperBack}>
            <Text style={styles.Back}>{'〈'}</Text>
          </View> */}
        </TouchableOpacity>
        <View>
          <Text style={styles.title}>{find?.title}</Text>
          <View style={styles.Rating}>
            <Ionicons name="star" size={13} color="yellow" />
            <Ionicons name="star" size={13} color="yellow" />
            <Ionicons name="star" size={13} color="yellow" />
            <Ionicons name="star" size={13} color="yellow" />
            <Ionicons name="star" size={13} color="white" />
          </View>
        </View>
      </ImageBackground>
      <View style={styles.ContainerDetail}>
        <View style={styles.WrapperDesc}>
          <Text style={styles.Sub}>Description Movie</Text>
        </View>
    
        <TouchableOpacity style={styles.ButtonPlay}>
          <Text style={styles.TextBtn}>Play Now</Text>
        </TouchableOpacity>

      </View>
    </View>
  );
};

export default DetailFilm;
