import {StyleSheet, Dimensions} from 'react-native';
import {scale} from 'react-native-size-matters';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
  imgs: {
    height: height * 0.42,
    width: width,
    resizeMode: 'cover',
    justifyContent: 'space-between',
    paddingLeft: scale(20),
    paddingBottom: scale(30),
    paddingRight: 8,
    paddingTop: scale(10),
  },
  title: {
    color: 'white',
    fontSize: scale(24),
    fontWeight: 'bold',
  },
  Sub: {
    color: 'white',
    fontSize: scale(17),
  },
  Rating: {
    flexDirection: 'row',
  },
  wrapper: {
    backgroundColor: 'red',
    height: '100%',
  },
  ImageStyle: {
    borderWidth: 0.2,
    backgroundColor: 'red',
    borderColor: '#d1d8e0',
  },
  Back: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 19,
  },
  WrapperBack: {
    color: 'white',
    width: 35,
    height: 35,
    justifyContent: 'center',
    borderRadius: 50,
    borderColor: 'white',
  },
  WrapperDesc: {
    paddingLeft: scale(12),
    paddingBottom: scale(10),
    paddingRight: 8,
    paddingTop: scale(20),
  },
  TextDesc: {
    color: 'white',
    fontSize: scale(14),
  },
  ContainerDetail: {
    flex: 1,
    backgroundColor: '#2d3436',
    borderTopRightRadius: scale(12),
    marginTop: scale(-15),
    borderColor: '#d1d8e0',
    borderTopLeftRadius: scale(12),
    borderWidth: 0.2,
    position: 'relative',
  },
  ButtonPlay: {
    backgroundColor: '#E50914',
    padding: 10,
    borderRadius: 12,
    margin: 'auto',
    width: width - 20,
    position: 'absolute',
    bottom: scale(8),
    alignSelf: 'center',
    alignItems: 'center',
  },
  TextBtn: {
    color: 'white',
    fontSize: 18,
    textAlign: 'center',
  }
});

export default styles;
