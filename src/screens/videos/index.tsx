import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import React from 'react';
import globalStyle from '../../styles/global';
import VideoPlayer from 'react-native-video-player';
// import video from '../../fakes/paws-of-fury.mov';
// import Thumb from '../../fakes/thumbnail.jpg';

const VideoScreens = () => {
  const [watch, setWatch] = React.useState<boolean>(false);
  return (
    <View style={globalStyle.Container}>
      <View style={styles.Container}>
        <Text style={styles.Title}>Favorite movie</Text>
        {watch ? (
          <VideoPlayer
            video={{
              uri: 'https://assets.mixkit.co/videos/preview/mixkit-aerial-view-of-city-traffic-at-night-11-large.mp4',
            }}
            videoWidth={1600}
            videoHeight={900}
          />
        ) : (
          <View style={styles.WrapImage}>
            <TouchableOpacity onPress={() => setWatch(true)}>
              <Text style={styles.Textt}>Play Now</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
      {watch ? (
        <View>
          <TouchableOpacity onPress={() => setWatch(false)}>
            <Text style={styles.Textt}>Stop</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <></>
      )}
    </View>
  );
};

export default VideoScreens;

const styles = StyleSheet.create({
  Container: {
    padding: 12,
  },
  Button: {
    backgroundColor: '#E50914',
    color: 'white',
    padding: 10,
    marginTop: 4,
    borderRadius: 4,
  },
  WrapImage: {
    width: '100%',
    height: 150,
    justifyContent: 'center',
    borderRadius: 6,
    backgroundColor: '#2d3436',
  },
  Textt: {
    color: '#d63031',
    textAlign: 'center',
    fontWeight: '800',
  },
  Title: {
    color: 'white',
    fontSize: 17,
    marginBottom: 7,
    fontWeight: 'bold',
  },
});
