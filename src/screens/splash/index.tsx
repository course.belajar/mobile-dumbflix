import {View, StyleSheet, Image} from 'react-native';
import React from 'react';
import {StackNavigationProp} from '@react-navigation/stack';
import {useNavigation} from '@react-navigation/native';
import {Tsplash} from '../../types/routetype/splash';
import Globalstyle from '../../styles/global';
import {Logo} from '../../assets/index';

type splashScreenProp = StackNavigationProp<Tsplash, 'main'>;

const SplashScreen = () => {
  const navigation = useNavigation<splashScreenProp>();

  setTimeout(() => {
    navigation.navigate('main');
  }, 3000);
  return (
    <View style={Globalstyle.Container}>
      <View style={styles.Wrapper}>
        <Image source={Logo} style={styles.Logo} />
      </View>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  Wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Logo: {},
});
