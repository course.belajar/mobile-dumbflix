import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import React from 'react';
import globalStyle from '../../styles/global';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import {Taccount} from '../../types/routetype/account';
// import {scale} from 'react-native-size-matters';
import {Logo} from '../../assets/index';

type RedirectscreenProps = StackNavigationProp<Taccount, 'login'>;
const RegisterScreen = () => {
  const navigation = useNavigation<RedirectscreenProps>();
  // const handleSign = () => {};
  return (
    <View style={globalStyle.Container}>
      <View style={styles.Container}>
        <View style={styles.Form}>
          <KeyboardAwareScrollView>
            <View style={styles.WrapperImage}>
              <View />
              <Image source={Logo} style={styles.img} />
              <View />
            </View>
            <View style={styles.WrapperInput}>
              <Text style={styles.Label}>Fulname</Text>
              <TextInput
                placeholder="Enter your Fulname"
                style={styles.Input}
                placeholderTextColor="#D2D2D240"
              />
            </View>
            <View style={styles.WrapperInput}>
              <Text style={styles.Label}>Username</Text>
              <TextInput
                placeholder="Enter your username"
                style={styles.Input}
                placeholderTextColor="#D2D2D240"
              />
            </View>
            <View style={styles.WrapperInput}>
              <Text style={styles.Label}>Email</Text>
              <TextInput
                placeholder="Enter your Email"
                style={styles.Input}
                placeholderTextColor="#D2D2D240"
              />
            </View>
            <View style={styles.WrapperInput}>
              <Text style={styles.Label}>Password</Text>
              <TextInput
                placeholder="Enter your password"
                style={styles.Input}
                placeholderTextColor="#D2D2D240"
              />
            </View>
            <TouchableOpacity style={styles.Button}>
              <Text style={styles.TextBtn}>Register</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('login')}>
              <Text style={styles.TextRegister}>
                Don't have an account ? Klik Here
              </Text>
            </TouchableOpacity>
          </KeyboardAwareScrollView>
        </View>
      </View>
    </View>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
    padding: 16,
    justifyContent: 'center',
  },
  Form: {},
  Input: {
    backgroundColor: 'rgba(210, 210, 210, 0.25)',
    borderRadius: 6,
    width: '100%',
  },
  Label: {
    fontSize: 14,
    color: 'white',
    marginBottom: 7,
  },
  WrapperInput: {
    marginBottom: 6,
  },
  Button: {
    backgroundColor: '#E50914',
    justifyContent: 'center',
    padding: 10,
    marginTop: 12,
    marginBottom: 5,
    borderRadius: 6,
    width: '100%',
  },
  TextBtn: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  TextRegister: {
    color: '#B1B1B1',
    textAlign: 'center',
  },
  img: {},
  WrapperImage: {
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
