import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import globalStyle from '../../styles/global';

const SearchScreen = () => {
  return (
    <View style={globalStyle.Container}>
      <Text style={styles}>SearchScreen</Text>
    </View>
  );
};

export default SearchScreen;

const styles = StyleSheet.create({});
