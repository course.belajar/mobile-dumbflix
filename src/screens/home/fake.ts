import {
  List1,
  List2,
  List3,
  List4,
  List7,
  List8,
  List6,
  List5,
} from '../../assets';

const datafilm = [
  {
    id: 1,
    title: 'Lorem ispum',
    img: List1,
    status: 1,
  },
  {
    id: 2,
    title: 'Dollor si amet',
    img: List2,
    status: 1,
  },
  {
    id: 3,
    title: 'random movies',
    img: List3,
    status: 0,
  },
  {
    id: 4,
    title: 'The Title',
    img: List4,
    status: 0,
  },
];
const datafilm2 = [
  {
    id: 1,
    title: 'Lorem ispum',
    img: List6,
    status: 1,
  },
  {
    id: 2,
    title: 'Dollor si amet',
    img: List7,
    status: 0,
  },
  {
    id: 3,
    title: 'random movies',
    img: List8,
    status: 1,
  },
  {
    id: 4,
    title: 'The Title',
    img: List5,
    status: 1,
  },
];

export {datafilm, datafilm2};
