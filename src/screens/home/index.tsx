/* eslint-disable react-native/no-inline-styles */
import React, {ReactNode} from 'react';
import {
  StyleSheet,
  View,
  LogBox,
  Text,
  FlatList,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import {scale} from 'react-native-size-matters';

import globalStyle from '../../styles/global';
import {Promobg, Promobg4, Promobg3, Logo, Promobg2} from '../../assets/index';
import {datafilm, datafilm2} from './fake';
import {Thome} from '../../types/routetype/home';
import SliderImage from '../../components/atoms/slider';
import CardItem from '../../components/atoms/card/item';
import {useNavigation} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

type RedirectscreenProps = StackNavigationProp<Thome, 'detail'>;

LogBox.ignoreLogs([
  'ViewPropTypes will be removed',
  'ColorPropType will be removed',
]);
const imgs = [Promobg2, Promobg4, Promobg3, Promobg];
const Genre = [
  {
    id: 1,
    name: 'Action',
    color: '#d63031',
  },
  {
    id: 2,
    name: 'Fantasy',
    color: '#f1c40f',
  },
  {
    id: 3,
    name: 'Romance',
    color: '#8e44ad',
  },
  {
    id: 4,
    name: 'Drama',
    color: '#0984e3',
  },
  {
    id: 5,
    name: 'Comedy',
    color: '#00b894',
  },
];

interface IHome {
  item: any;
  HomeScreen: ReactNode;
  HandleDetail: (item: any) => void;
}
const HomeScreen: React.FC<IHome> = () => {
  const navigation = useNavigation<RedirectscreenProps>();
  const HandleDetail = (event: any) => {
    navigation.navigate('detail', {
      userId: event.id,
    });
  };
  const GenreList = ({item}: any) => {
    return (
      <TouchableOpacity style={{marginBottom: scale(12)}}>
        <View
          style={{
            backgroundColor: item.color,
            margin: 5,
            paddingLeft: 12,
            paddingRight: 12,
            paddingTop: 5,
            paddingBottom: 5,
            borderRadius: 6,
          }}>
          <Text style={{letterSpacing: 2, color: 'white', fontWeight: 'bold'}}>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={globalStyle.Container}>
      <View style={styles.WrapperHead}>
        <View />
        <Image source={Logo} style={styles.img} />
        <View />
      </View>
      <SliderImage imgs={imgs} />
      <ScrollView style={styles.Content}>
        <View style={styles.WrapperSub}>
          <Text style={styles.TitleMovie}>List Genre</Text>
        </View>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={Genre}
          renderItem={({item}) => <GenreList item={item} key={item.id} />}
        />
        <View style={styles.WrapperSub}>
          <Text style={styles.TitleMovie}>Movies</Text>
        </View>
        <FlatList
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          data={datafilm2}
          renderItem={({item}) => (
            <CardItem
              item={item}
              key={item.id}
              onDetail={() => HandleDetail(item)}
            />
          )}
        />
        <View style={styles.WrapperSub}>
          <Text style={styles.TitleMovie}>TV Series</Text>
        </View>
        <FlatList
          horizontal={true}
          data={datafilm}
          renderItem={({item}) => (
            <CardItem
              item={item}
              key={item.id}
              onDetail={() => HandleDetail(item)}
            />
          )}
        />
      </ScrollView>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  TextIcon: {
    color: 'red',
    fontSize: scale(20),
    fontWeight: 'bold',
    textAlign: 'center',
    margin: scale(4),
    letterSpacing: 4,
  },
  TitlePromo: {
    fontSize: scale(25),
    color: 'white',
    fontWeight: 'bold',
  },
  Icon: {
    paddingLeft: scale(20),
    paddingRight: scale(20),
    backgroundColor: '#E50914',
    color: 'white',
  },
  Content: {
    padding: scale(6),
  },
  title: {
    color: '#eb4d4b',
    fontSize: scale(20),
    paddingTop: scale(6),
    paddingBottom: scale(6),
    fontWeight: 'bold',
    marginBottom: scale(12),
    letterSpacing: scale(2),
  },
  img: {},
  Wrappercard: {
    flexDirection: 'row',
  },
  TitleMovie: {
    color: 'white',
    fontWeight: '600',
    fontSize: scale(20),
  },
  TextPromo: {
    color: 'white',
    fontSize: 12,
    fontStyle: 'normal',
  },
  WrapperSub: {
    marginBottom: scale(8),
  },
  WrapperHead: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingTop: scale(4),
  },
});
