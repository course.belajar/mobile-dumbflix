import LoginScreen from './login';
import RegisterScreen from './register';
import SplashScreen from './splash';
import HomeScreen from './home';
import DetailFilm from './detail';
import SearchScreen from './search';
import AccountScreen from './account';
import VideoScreens from './videos';

export {
  LoginScreen,
  RegisterScreen,
  SplashScreen,
  HomeScreen,
  DetailFilm,
  SearchScreen,
  AccountScreen,
  VideoScreens,
};
