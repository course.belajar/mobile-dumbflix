import List1 from './im.png';
import List2 from './im2.png';
import List3 from './im3.png';
import List4 from './img4.png';
import List5 from './im7.png';
import List6 from './im8.png';
import List7 from './im9.png';
import List8 from './im10.png';
import List9 from './im11.png';
import Promobg from './promo.png';
import Promobg2 from './promo2.png';
import Promobg3 from './promo3.png';
import Promobg4 from './promo5.jpg';
import Logo from './logo.png';

export {
  List1,
  List2,
  List3,
  List4,
  List5,
  List8,
  List6,
  List7,
  List9,
  Promobg,
  Promobg2,
  Promobg3,
  Promobg4,
  Logo,
};
