import React from 'react';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  Text,
} from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const SliderImage = ({imgs}: any) => {
  const imglenggth = imgs.length - 1;
  const [index, setindex] = React.useState<any>(0);
  const handleBack = React.useCallback(() => {
    if (index === 0) {
      setindex(imglenggth);
    } else {
      setindex(index - 1);
    }
  }, [setindex, imglenggth, index]);
  const handleNext = React.useCallback(() => {
    if (index === imglenggth) {
      setindex(0);
    } else {
      setindex(index + 1);
    }
  }, [setindex, imglenggth, index]);
  return (
    <View style={styles.PromoBg}>
      <ImageBackground source={imgs[index]} style={styles.backgroundImage}>
        <View style={styles.Wrapper}>
          <TouchableOpacity onPress={handleBack}>
            <Text style={styles.Arrow}>{'〈'}</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={handleNext}>
            <Text style={styles.Arrow}>{'〉'}</Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
};

export default SliderImage;

const styles = StyleSheet.create({
  PromoBg: {},
  backgroundImage: {
    height: height * 0.25,
    width: width,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  Wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  Arrow: {
    fontSize: 15,
    color: '#ecf0f1',
    paddingLeft: 3,
    paddingRight: 3,
    fontWeight: 'bold',
  },
});
