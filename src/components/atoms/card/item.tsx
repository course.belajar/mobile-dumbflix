import {StyleSheet, Text, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {scale} from 'react-native-size-matters';

interface Icard {
  item: any;
  onDetail: (item: any) => void;
}

const CardItem: React.FC<Icard> = ({item, onDetail}) => {
  return (
    <TouchableOpacity style={styles.Container} onPress={() => onDetail(item)}>
      <Image source={item.img} style={styles.ContainerImg} />
      <Text style={styles.title}>{item.title}</Text>
      <Text style={styles.Years}>1998</Text>
    </TouchableOpacity>
  );
};

export default CardItem;

const styles = StyleSheet.create({
  Container: {
    borderRadius: 10,
    width: scale(150),
    margin: 2,
    marginBottom: scale(8),
    padding: 2,
    justifyContent: 'center',
  },
  ContainerImg: {
    resizeMode: 'cover',
    width: scale(146),
    borderRadius: 6,
    height: scale(180),
    margin: 'auto',
  },
  title: {
    color: 'white',
    fontSize: 15,
    fontWeight: 'bold',
  },
  img: {
    backgroundColor: 'gray',
  },
  Years: {
    color: 'white',
    fontSize: 14,
  },
});
