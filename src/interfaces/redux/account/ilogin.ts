export enum ActionType {
  ACTION_LOGIN_BEGIN = 'ACTION_LOGIN_BEGIN',
  ACTION_LOGIN_SUCCESS = 'ACTION_LOGIN_SUCCESS',
  ACTION_LOGIN_ERROR = 'ACTION_LOGIN_ERROR',
}

interface IactionLoginBegin {
  type: ActionType.ACTION_LOGIN_BEGIN;
}

interface IactionLoginSuccess {
  type: ActionType.ACTION_LOGIN_SUCCESS;
  success: any;
}

interface actionLoginFail {
  type: ActionType.ACTION_LOGIN_ERROR;
  error: any;
}

export type ActionLogin =
  | IactionLoginBegin
  | IactionLoginSuccess
  | actionLoginFail;
