declare module '*.jpg';
declare module '*.jpeg';
declare module '*.png';
declare module '*.mov';
declare module '*.mp4';
declare module '*.env';
declare module '@env' {
  export const API_DEV: string;
}
declare module '*AsyncStorage';
