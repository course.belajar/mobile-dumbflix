import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Thome} from '../../types/routetype/home';
import {HomeScreen, DetailFilm} from '../../screens';

const Stack = createNativeStackNavigator<Thome>();
const HomeTab = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="default" component={HomeScreen} />
      <Stack.Screen name="detail" component={DetailFilm} />
    </Stack.Navigator>
  );
};

export default HomeTab;
