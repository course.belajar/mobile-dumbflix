import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Taccount} from '../../types/routetype/account';
import {AccountScreen, LoginScreen, RegisterScreen} from '../../screens';

const Stack = createNativeStackNavigator<Taccount>();

const AccountTab = () => {
  const [auth] = React.useState<boolean>(false);
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {auth ? (
        <>
          <Stack.Screen name="default" component={AccountScreen} />
        </>
      ) : (
        <>
          <Stack.Screen name="login" component={LoginScreen} />
          <Stack.Screen name="register" component={RegisterScreen} />
        </>
      )}
    </Stack.Navigator>
  );
};

export default AccountTab;
