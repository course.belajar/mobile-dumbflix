import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Tsearch} from '../../types/routetype/search';
import {SearchScreen} from '../../screens';

const Stack = createNativeStackNavigator<Tsearch>();
const SearchTab = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="default" component={SearchScreen} />
    </Stack.Navigator>
  );
};

export default SearchTab;
