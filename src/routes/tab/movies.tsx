import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Tmovie} from '../../types/routetype/movies';
import {VideoScreens} from '../../screens';

const Stack = createNativeStackNavigator<Tmovie>();
const MovieTab = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="default" component={VideoScreens} />
    </Stack.Navigator>
  );
};

export default MovieTab;
