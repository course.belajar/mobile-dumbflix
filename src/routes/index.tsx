import React from 'react';
import {MainApp} from '../types/routetype/route';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

// Bottom Tab
import AccountTab from './tab/account';
import HomeTab from './tab/home';
import SearchTab from './tab/search';
import MovieTab from './tab/movies';

const Tab = createBottomTabNavigator<MainApp>();

const Route = () => {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        headerShown: false,
        tabBarStyle: {
          backgroundColor: '#222f3e',
          paddingLeft: 8,
        },
        tabBarHideOnKeyboard: true,
        tabBarIcon: ({focused, color, size}) => {
          let iconName: any;
          if (route.name === 'home') {
            iconName = focused ? 'home' : 'home-outline';
          } else if (route.name === 'search') {
            iconName = focused ? 'search' : 'search-outline';
          } else if (route.name === 'videos') {
            iconName = focused ? 'heart' : 'heart-outline';
          } else if (route.name === 'account') {
            iconName = focused ? 'person-circle' : 'person-circle-outline';
          }
          return <Ionicons name={iconName} size={size} color={color} />;
        },
        tabBarActiveTintColor: '#E50914',
        tabBarInactiveTintColor: '#b2bec3',
      })}>
      <Tab.Screen name="home" component={HomeTab} />
      <Tab.Screen name="search" component={SearchTab} />
      <Tab.Screen name="videos" component={MovieTab} />
      <Tab.Screen name="account" component={AccountTab} />
    </Tab.Navigator>
  );
};

export default Route;
