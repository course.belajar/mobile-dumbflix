import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Trouter} from './types/routetype/route';
import {SplashScreen} from './screens';
import RouteMain from './routes/index';

const Stack = createNativeStackNavigator<Trouter>();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="splash" component={SplashScreen} />
        <Stack.Screen name="main" component={RouteMain} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
