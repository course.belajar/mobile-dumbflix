import {StyleSheet} from 'react-native';

const globalStyle = StyleSheet.create({
  Container: {
    backgroundColor: '#090A09',
    color: '#15f6fa',
    flex: 1,
  },
  Logo: {
    color: '#E50914',
    fontSize: 23,
    fontWeight: 'bold',
    letterSpacing: 2,
    textAlign: 'center',
    marginBottom: 1,
  },
});

export default globalStyle;
